(function($) {
  Drupal.behaviors.blockCollapse = {
    attach: function(context, settings) {
      var settings = $.extend(Drupal.behaviors.blockCollapse.defaults, settings['blockCollapse'] || {});
      
      $('.block-collapse', context).each(function() {
        var block = $(this);
        var title = $('h2.block-title', block).addClass('collapsed');
        var content = $('.block-collapse-content', block).hide();
        
        var state = 'collapsed';
        
        function expand() {
          state = 'expanding';
          block.trigger('expanding.block');
          
          title.removeClass('collapsed').addClass('expanded');
          content.slideDown(settings.speed, function() {
            state = 'expanded';
            block.trigger('expanded.block');
          });
        };
      
        function collapse() {
          state = 'collasping';
          block.trigger('collapsing.block');
          
          title.removeClass('expanded').addClass('collapsed');
          content.slideUp(settings.speed, function() {
            state = 'collapsed';
            block.trigger('collapsed.block');
          });
        }

        block.hoverIntent(expand, collapse);
      
        // Handle clicks as well, to support touch devices
        title.click(function() {
          if (state === 'collapsed') {
            expand();
          }
          else if (state === 'expanded') {
            collapsed();
          }
          return false;
        });
      });
    },
    defaults: {
      speed: 400
    }
  };
})(jQuery);

